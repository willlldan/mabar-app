/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

// Route.get('/', async () => {
//   return { hello: 'world' }
// })

Route.group(() => {
  // Auth
  Route.post('/register', 'AuthController.register')
  Route.post('/resend-otp', 'AuthController.resendOtp')
  Route.post('/otp-confirmation', 'AuthController.otpConfirmation')
  Route.post('/login', 'AuthController.login').middleware(['verify'])

  Route.group(() => {
    Route.resource('/venue', 'VenuesController').middleware({'*' : 'owner'})
    Route.get('/venue/:id/fields', 'VenuesController.getAllFields')
    Route.get('/venue/:id/fields/:fieldId', 'VenuesController.getField')
    Route.resource('/field', 'FieldsController')
    Route.post('/field/:id/booking', 'FieldsController.booking')

    Route.group(() => {
      Route.get('/', 'BookingsController.index')
      Route.get('/schedule', 'BookingsController.schedule')
      Route.get('/:id', 'BookingsController.show')
      Route.post('/:id/join', 'BookingsController.join')
      Route.post('/:id/unjoin', 'BookingsController.unjoin')
    }).prefix('/booking').middleware(['user'])

  }).middleware(['auth'])

}).prefix('/api/v1')
