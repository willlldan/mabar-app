import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class Verify {
  public async handle({request, response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL

    let user = await User.findByOrFail('email', request.input('email'))
    let isActive = await user.isActive;

    if(isActive) {
      await next()
    } else {
      return response.unauthorized({messages: "You have not verified"})
    }
  }
}
