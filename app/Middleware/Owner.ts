import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Owner {
  public async handle({auth, response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let user = auth.user?.role

    if(user == 'owner') {
      await next()
    } else {
      return response.unauthorized({messages: "Only owner can access this"})
    }
  }
}
