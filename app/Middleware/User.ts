import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class User {
  public async handle({auth, response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let user = auth.user?.role
    
    if(user == 'user') {
      await next()
    } else {
      return response.unauthorized({messages: "Only owner can access this"})
    }
    await next()
  }
}
