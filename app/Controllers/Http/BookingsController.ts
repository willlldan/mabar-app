import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import User from 'App/Models/User'
import Database from '@ioc:Adonis/Lucid/Database';

export default class BookingsController {

    public async index({response}:HttpContextContract) {
        let booking = await Booking.all();

        response.status(200).json({messages: "Successfully get data", data: booking})
    }

    public async show({response, params}:HttpContextContract) {
        let booking = await Booking.findOrFail(params.id)

        await booking.load('users')

        response.status(200).json({messages: "Successfully get data", data: booking})
    }


    public async join({auth, response, params}:HttpContextContract) {
    
        let userId = auth.user?.id
        let booking = await Booking.findOrFail(params.id)
        let user = await User.findOrFail(userId)

         // cek already booking
         let user_booking = await Database.query()
         .from('user_booking')
         .count('* as total')
         .where('booking_id', booking.id)
         .andWhere('user_id', user.id)
         .limit(1)
         .first()

         if(user_booking.total) {
            return response.ok({messages: 'User already booking'})
         }
        
        try {
            booking.related('users').attach([user.id])
            response.created({messages: 'success join booking'})
        } catch (error) {
            response.badRequest({error: "Failed join"})
        }

    }
    
    public async unjoin({auth, response, params}:HttpContextContract) {
        let userId = auth.user?.id
        let booking = await Booking.findOrFail(params.id)
        let user = await User.findOrFail(userId)
        
        try {
            booking.related('users').detach([user.id])
            response.created({messages: 'success unjoin booking'})
        } catch (error) {
            response.badRequest({error: "Failed unjoin"})
        }
    }

    public async schedule({response, auth}:HttpContextContract) {
        let user = await User.findOrFail(auth.user?.id)
        let booking = await Database.query()
                    .select('bookings.id', 'fields.name', 'fields.type', 
                            'bookings.play_date_start', 'bookings.play_date_end')
                    .from('user_booking')
                    .where('user_booking.user_id', user.id)
                    .join('bookings', 'user_booking.booking_id', 'bookings.id')
                    .join('fields', 'bookings.field_id', 'fields.id')
        return response.ok({messages: "Successfully get data", data: booking})
    }


}
