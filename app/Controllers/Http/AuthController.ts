import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UserValidator from 'App/Validators/UserValidator';
import User from 'App/Models/User';
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database';
import { schema, rules } from '@ioc:Adonis/Core/Validator'

export default class AuthController {
    public async register({request, response} : HttpContextContract) {
        try {
            const data = await request.validate(UserValidator);
            let newUser = new User();
            newUser.name = data.name
            newUser.email = data.email
            newUser.password = data.password
            newUser.role = request.input('role')
            await newUser.save()

            const otp_code = Math.floor(100000 + Math.random() * 900000)
            await Database.table('otp_codes').insert({otp_code: otp_code, user_id: newUser.id})

            await Mail.send((message) => {
                message
                  .from('adonis.demo@sanberdev.com')
                  .to(newUser.email)
                  .subject('Welcome Onboard!')
                  .htmlView('emails/otp_verification', { otp_code })
              })
            return response.created({messages: "Register success, check your email for verify account"})
        } catch (error) {
            return response.unprocessableEntity({messages: error.messages});
        }
    }

    public async resendOtp({request, response} : HttpContextContract) {
        const resendValidator = schema.create({email: schema.string({}, [rules.email()])})
        await request.validate({schema:resendValidator})

        let email = request.input('email');
        let user = await User.findByOrFail('email', email)
        
        if (user.isActive) {
            return response.status(422).json({messages: 'already verified'})
        }

        const otp_code = Math.floor(100000 + Math.random() * 900000)
        await Database.from('otp_codes').where('user_id', user.id).update({otp_code: otp_code})

        await Mail.send((message) => {
            message
              .from('adonis.demo@sanberdev.com')
              .to(user.email)
              .subject('Welcome Onboard!')
              .htmlView('emails/otp_verification', { otp_code })
          })

          return response.created({messages: "Check your email for verify account"})
    }

    public async otpConfirmation({request, response}) {
        const confirmValidator = schema.create({
            email: schema.string({}, [rules.email()]),
            otp_code: schema.string()
        })
        await request.validate({schema:confirmValidator})

        let user = await User.findByOrFail('email', request.input('email'))
        let otpCheck = await Database.query().from('otp_codes').where('user_id',user.id).first()

        if(otpCheck.otp_code == request.input('otp_code')) {
            user.isActive = true;
            await user.save()
            return response.ok({messages: "Hooray, Verification Success" })
        } else {
            return response.status(400).json({messages: "OTP code is wrong"})
        }
    }

    public async login({request, response, auth}: HttpContextContract) {
        try {

            const userSchema = schema.create({
                email : schema.string(),
                password: schema.string()
            })
            await request.validate({schema: userSchema});
            const email = request.input('email')
            const password = request.input('password')

            const token = await auth.use('api').attempt(email, password)
            return response.ok({messages: 'login succes', token})
        } catch (error) {
            const messages = error.guard? error.message : error.messages
            return response.badRequest({messages})
        }
    }

}
