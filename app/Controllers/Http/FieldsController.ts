import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import FieldValidator from 'App/Validators/FieldValidator'
import Field from 'App/Models/Field';
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User';
import Booking from 'App/Models/Booking';


export default class FieldsController {
  public async index({response}: HttpContextContract) {
    let fields = await Field.all()
    response.status(200).json({messages: "Successfully get data", data: fields})
  }

  public async store({request, response}: HttpContextContract) {
    await request.validate(FieldValidator)

    let newField = new Field();
    newField.name = request.input('name')
    newField.type = request.input('type')
    newField.venueId = request.input('venueId')

    try {
      await newField.save()
      response.created({messages: "Successfully Added"})
    } catch (error) {
      response.badRequest(error)
    }

  }

  public async show({params, response}: HttpContextContract) {
    let field = await Field.findOrFail(params.id)

    // await field.load('bookings')

    response.status(200).json({messages: "success get field", data : field})
  }

  public async update({request, response, params}: HttpContextContract) {
    let field = await Field.findOrFail(params.id)
    field.name = request.input('name')
    field.type = request.input('type')
    field.venueId = request.input('venueId')

    await field.save()
    response.status(200).json({messages: "Successfully Updated!"})
  }

  public async destroy({params, response}: HttpContextContract) {
    await (await Field.findOrFail(params.id)).delete()
    response.status(200).json({messages: "Successfully Deleted!"})
  }

  public async booking({request, response, params, auth}:HttpContextContract) {
    const bookingValidator = schema.create({
      play_date_start: schema.date({
        format: 'yyyy-MM-dd hh:mm:ss'
      }, [
        rules.after('today'),
        rules.unique({ table: 'bookings', column: 'play_date_start' })
      ]),
      play_date_end: schema.date({
        format: 'yyyy-MM-dd hh:mm:ss'
      }, [
        rules.after('today'),
        rules.unique({ table: 'bookings', column: 'play_date_end' }),
        rules.afterField('play_date_start')
      ])
    })
    
    await request.validate({schema: bookingValidator})
    let field = await Field.findOrFail(params.id)
    let user = await User.findOrFail(auth.user?.id)

    const booking = new Booking();
    booking.play_date_start = request.input('play_date_start')
    booking.play_date_end = request.input('play_date_end')

    await booking.related('user').associate(user)
    await field.related('bookings').save(booking)
    
    // add to user_booking
    let userBooking = await Booking.query().orderBy('id', 'desc').first()
    await userBooking?.related('users').attach([user.id])

    response.created({messages: "Successfully booking"})
  }

}
