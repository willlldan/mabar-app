import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import VenueValidator from 'App/Validators/VenueValidator';
import Venue from 'App/Models/Venue';

export default class VenuesController {
  public async index({response}: HttpContextContract) {
        let venues = await Venue.query().preload('fields')
        response.status(200).json({messages: "success get venues", data : venues})
  }

  public async store({request, response, auth}: HttpContextContract) {
    await request.validate(VenueValidator)

    if (auth.user?.role != 'owner') {
      return response.status(403).json({messages: "Only owner can add venue"})
    }

        let newVenue = new Venue();
        newVenue.name = request.input('name')
        newVenue.address = request.input('address')
        newVenue.phone = request.input('phone')
        newVenue.userId = auth.user?.id

        try {
            await newVenue.save()
            response.created({messages: "Successfully added!"})
        } catch (error) {
            response.badRequest(error)
        }
  }

  public async show({response, params}: HttpContextContract) {
    let venues = await Venue.findByOrFail('id', params.id)

    await venues.load('fields')

    response.status(200).json({messages: "success get venues", data : venues})
  
  }

  public async update({request, response, params, auth}: HttpContextContract) {

    let venue = await Venue.findOrFail(params.id)

    if(venue.userId != auth.user?.id) {
      return response.badRequest({messages: 'Only owners venue can update this venue'})
    }

    venue.name = request.input('name')
    venue.address = request.input('address')
    venue.phone = request.input('phone')
  
    try {
        venue.save()
        response.status(200).json({messages: "Successfully Updated!"})
    } catch (error) {
        response.badRequest(error)
    }

  }

  public async destroy({response, params}: HttpContextContract) {
    (await Venue.findOrFail(params.id)).delete()
    response.status(200).json({messages: "Successfully Deleted!"})
  }

  public async getAllFields({response, params}: HttpContextContract) {
    let venues = await Venue.findByOrFail('id', params.id)

    await venues.load('fields')

    response.status(200).json({messages: "success get venues", data : venues.fields})
  }

  public async getField({response, params}: HttpContextContract) {
    let venues = await Venue.findByOrFail('id', params.id)

    await venues.load('fields')

    let field = venues.fields.find(field => field.id == params.fieldId)

    response.status(200).json({messages: "success get venues", data : field})
  }
}
